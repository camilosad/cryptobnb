var Property = artifacts.require("./Property.sol");
var PropertyToken = artifacts.require("./PropertyToken.sol");
var PropertyRegistry = artifacts.require("./PropertyRegistry.sol");

module.exports = function(deployer) {
  let propertyAddress, propertyTokenAddress;

  deployer.deploy(Property)
    .then(() => Property.deployed())
    .then(property => propertyAddress = property.address)
    .then(() => deployer.deploy(PropertyToken))
    .then(() => PropertyToken.deployed())
    .then(propertyToken => propertyTokenAddress = propertyToken.address)
    .then(() => deployer.deploy(PropertyRegistry, propertyAddress, propertyTokenAddress))
};
