var Property = artifacts.require("Property");
var PropertyToken = artifacts.require("PropertyToken");
var PropertyRegistry = artifacts.require("PropertyRegistry");

contract('PropertyRegistry', function(accounts) {
  const alice = accounts[0];
  const bob = accounts[1];
  const eve = accounts[2];
  
  const setup = async () => {
    const propertyToken = await PropertyToken.new();
    const property = await Property.new();
    const uri = 'http://myproperty.ca';
    await property.createProperty(uri, { from: alice });
    const tokenId = await property.tokenOfOwnerByIndex(alice, 0);
    const propertyRegistry = await PropertyRegistry.new(property.address, propertyToken.address);
    return { tokenId, propertyRegistry };
  }

  const newRequestFrom = async (addr) => {
    const { tokenId, propertyRegistry } = await setup();
    const now = new Date().getTime();
    const checkin = now;
    const checkout = checkin + 1000000;
    await propertyRegistry.request(tokenId, checkin, checkout, { from: addr });
    return { tokenId, propertyRegistry };
  }

  // PROPERTY REGISTRATION

  it("should allow alice to register her property", async () => {
    const { tokenId, propertyRegistry } = await setup();
    await propertyRegistry.registerProperty(tokenId, 100, { from: alice });
    const totalProperties = await propertyRegistry.totalProperties();
    assert(totalProperties.toNumber() == 1, "Property not registered");
    const registeredProperties = await propertyRegistry.getRegisteredProperties({ from: bob });
    assert(registeredProperties[0], "Property not registered");
  });

  it("should not allow bob to register alices property", async () => {
    const { tokenId, propertyRegistry } = await setup();
    try {
      await propertyRegistry.registerProperty(tokenId, 100, { from: bob });
    }
    catch (error) { /* transaction was reverted as expected */ }
    const totalProperties = await propertyRegistry.totalProperties();
    assert(totalProperties.toNumber() == 0, "Property was registered");
  });


  // REQUESTS

  it("should allow bob to request alices property", async () => {
    const { tokenId, propertyRegistry } = await newRequestFrom(bob);
    const status = await propertyRegistry.getRequestStatus(tokenId, { from: bob });
    assert(status == "pending", "Could not request property");
  });

  it("should allow bob to get a list of his requested properties", async () => {
    const { tokenId, propertyRegistry } = await newRequestFrom(bob);
    const properties = await propertyRegistry.getRequestedProperties({ from: bob });
    assert(properties.length > 0, "Bob could not get the list");
  });

  it("should add new requests to the pendingRequests list", async () => {
    const { tokenId, propertyRegistry } = await newRequestFrom(bob);
    const requests = await propertyRegistry.getPendingRequests(tokenId);
    assert(requests[0] === bob, "Request was not added to the list");
  });

  it("should allow alice to approve bobs request", async () => {
    const { tokenId, propertyRegistry } = await newRequestFrom(bob);
    await propertyRegistry.approveRequest(tokenId, bob, { from: alice });
    const status = await propertyRegistry.getRequestStatus(tokenId, { from: bob });
    assert(status == "approved", "Request was not approved");
  });

  it("should add approved requests to the list", async () => {
    const { tokenId, propertyRegistry } = await newRequestFrom(bob);
    await propertyRegistry.approveRequest(tokenId, bob, { from: alice });
    const requests = await propertyRegistry.getApprovedRequests(tokenId, { from: bob });
    assert(requests.length > 0 , "Request was not added to the list");
  });

  it("should allow alice to reject bobs request", async () => {
    const { tokenId, propertyRegistry } = await newRequestFrom(bob);
    await propertyRegistry.rejectRequest(tokenId, bob, { from: alice });
    const status = await propertyRegistry.getRequestStatus(tokenId, { from: bob });
    assert(status == "rejected", "Request was not rejected");
  });

  
  // CHECKIN

  it("should not allow another person to checkin", async () => {
    const { tokenId, propertyRegistry } = await newRequestFrom(bob);
    try {
      await propertyRegistry.checkin(tokenId, { from: eve });
    }
    catch (error) { /* transaction was reverted as expected */ }
    const stays = await propertyRegistry.getPropertyStays(tokenId, { from: alice });
    assert(stays == 0, "eve was able to checkin");
  });

  it("should allow bob to checkin once the request is approved and time has come", async () => {
    const { tokenId, propertyRegistry } = await newRequestFrom(bob);
    await propertyRegistry.approveRequest(tokenId, bob, { from: alice });
    await propertyRegistry.checkin(tokenId, { from: bob });
    const stays = await propertyRegistry.getPropertyStays(tokenId, { from: alice });
    assert(stays == 1, "bob was not able to checkin");
    const occupant = await propertyRegistry.getPropertyOccupant(tokenId, { from: alice });
    assert(occupant == bob, "Checkin was not allowed");
  });

  it("should not allow bob to checkin if the request has not been approved", async () => {
    const { tokenId, propertyRegistry } = await newRequestFrom(bob);
    try {
      await propertyRegistry.checkin(tokenId, { from: bob });
    }
    catch (error) { /* transaction was reverted as expected */ }
    const stays = await propertyRegistry.getPropertyStays(tokenId, { from: alice });
    assert(stays == 0, "bob was able to checkin");
  });

  // it("should not allow bob to checkin before the time", async () => {
  //   const { tokenId, propertyRegistry } = await setup();
  //   const now = new Date().getTime();
  //   const checkin = now + 1000000;
  //   const checkout = checkin + 1000000;
  //   await propertyRegistry.request(tokenId, checkin, checkout, { from: bob });
  //   await propertyRegistry.approveRequest(tokenId, bob, { from: alice });
  //   try {
  //     await propertyRegistry.checkin(tokenId, { from: bob });
  //   }
  //   catch (error) { /* transaction was reverted as expected */ }
  //   const stays = await propertyRegistry.getPropertyStays(tokenId, { from: alice });
  //   assert(stays == 0, "bob was able to checkin");
  // });

  // it("should transfer token from bob to registry once bob is checked in", async () => {
  //   const { tokenId, propertyRegistry } = await setup();
  //   const now = new Date().getTime();
  //   const checkin = now;
  //   const checkout = checkin + 1000000;
  //   await propertyRegistry.request(tokenId, checkin, checkout, { from: bob });
  //   await propertyRegistry.approveRequest(tokenId, bob, { from: alice });
  //   await propertyRegistry.checkin(tokenId, { from: bob });
  //   // check bobs balance (- price)
  //   // check registry balance (+ price)
  // });


  // CHECKOUT

  it("should allow bob to checkout", async () => {
    const { tokenId, propertyRegistry } = await setup();
    const now = new Date().getTime();
    const checkin = now;
    const checkout = checkin + 1000000;
    await propertyRegistry.request(tokenId, checkin, checkout, { from: bob });
    await propertyRegistry.approveRequest(tokenId, bob, { from: alice });
    await propertyRegistry.checkin(tokenId, { from: bob });
    await propertyRegistry.checkout(tokenId, { from: bob });
    const occupant = await propertyRegistry.getPropertyOccupant(tokenId, { from: alice });
    assert(occupant != bob, "bob could not checkout");
  });

  it("should not allow eve to checkout when bob is the occupant", async () => {
    const { tokenId, propertyRegistry } = await setup();
    const now = new Date().getTime();
    const checkin = now;
    const checkout = checkin + 1000000;
    await propertyRegistry.request(tokenId, checkin, checkout, { from: bob });
    await propertyRegistry.approveRequest(tokenId, bob, { from: alice });
    await propertyRegistry.checkin(tokenId, { from: bob });
    try {
      await propertyRegistry.checkout(tokenId, { from: eve });
    }
    catch (error) { /* transaction was reverted as expected */ }
    const occupant = await propertyRegistry.getPropertyOccupant(tokenId, { from: alice });
    assert(occupant == bob, "eve checked bob out");
  });

  // it("should transfer the tokens to alice once bob is checked out", async () => {
  //   const { tokenId, propertyRegistry } = await setup();
  //   const now = new Date().getTime();
  //   const checkin = now;
  //   const checkout = checkin + 1000000;
  //   await propertyRegistry.request(tokenId, checkin, checkout, { from: bob });
  //   await propertyRegistry.approveRequest(tokenId, bob, { from: alice });
  //   await propertyRegistry.checkin(tokenId, { from: bob });
  //   await propertyRegistry.checkout(tokenId, { from: bob });
  //   // check new registry balance (- price)
  //   // check alices balance (+ price)
  // });
  

});
