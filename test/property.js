var Property = artifacts.require("Property");

contract('Property', function(accounts) {
  const alice = accounts[0];
  const jack = accounts[1];
  
  const setup = async () => {
    const property = await Property.new();
    const uri = 'http://myproperty1.ca';
    await property.createProperty(uri, { from: alice });
    return property;
  }

  it("should allow Alice to create a property with a uri and give her a tokenId", async () => {
    const property = await setup();
    const count = await property.totalSupply();
    assert(count.toNumber() === 1, 'Property was not created');
    const tokenId = await property.tokenOfOwnerByIndex(alice, 0)
    assert(tokenId, 'Property was not created');
    const tokenURI = await property.tokenURI(tokenId);
    assert(tokenURI === 'http://myproperty1.ca', 'Property was not created');
  });


  it("should allow Alice to set her token uri", async () => {
    const property = await setup();
    await property.setURI(1, 'http://my.house.com');
    const tokenURI = await property.tokenURI(1);
    assert(tokenURI === 'http://my.house.com', 'Token was not set');
  });


  it("should not allow Jack to set Alice's token uri", async () => {
    const property = await setup();
    try {
      await property.setURI(1, 'http://my.house.com', { from: jack });
    }
    catch (error) { /* transaction was reverted as expected */ }
    const tokenURI = await property.tokenURI(1);
    assert(tokenURI !== 'http://my.house.com', 'Token was set without permission');
  });
});
