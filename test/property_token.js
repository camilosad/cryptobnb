var Property = artifacts.require("Property");
var PropertyToken = artifacts.require("PropertyToken");
var PropertyRegistry = artifacts.require("PropertyRegistry");

contract('PropertyToken', function(accounts) {
  const alice = accounts[0];
  const bob = accounts[1];

  it('should allow alice to mint Property Token for bob', async () => {
    const propertyToken = await PropertyToken.new();
    await propertyToken.mint(bob, 200);
    const balance = await propertyToken.balanceOf.call(bob);
    assert(balance.toNumber() === 200, 'balance');
  });

  it('should allow bob to approve the property registry to use his tokens', async () => {
    const property = await Property.new();
    const propertyToken = await PropertyToken.new();
    const propertyRegistry = await PropertyRegistry.new(property.address, propertyToken.address);
    const tx = await propertyToken.approve(propertyRegistry.address, 200, { from: bob });
    assert(tx !== undefined, 'property registry has not been approved');
  });
  
});
