pragma solidity ^0.4.22;

import 'openzeppelin-solidity/contracts/token/ERC20/DetailedERC20.sol';
import 'openzeppelin-solidity/contracts/token/ERC20/MintableToken.sol';

contract PropertyToken is DetailedERC20, MintableToken {
  
  constructor() DetailedERC20("PropertyToken", "PTOKEN", 18) public {}
}
