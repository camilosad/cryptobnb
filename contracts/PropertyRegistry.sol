pragma solidity ^0.4.22;

import "./Property.sol";
import "./PropertyToken.sol";

contract PropertyRegistry {

  struct Data {
    uint256 price;
    uint256 stays;
    bool registered;
    address occupant;
    address[] pendingRequests;
    address[] approvedRequests;
    mapping(address => uint256) pendingRequestsIndexes;
    mapping(address => uint256) approvedRequestsIndexes;
    mapping(address => Request) requestByAddress;
  }

  struct Request {
    string status; // pending, approved, rejected, checkedIn, checkedOut, canceled
    uint256 checkin;
    uint256 checkout;
  }

  Property public property;
  PropertyToken public propertyToken;
  mapping(uint256 => Data) public propertyData;
  mapping(address => uint256[]) public requestedPropertiesByAddress;
  uint256[] public registeredProperties;

  constructor(address _property, address _propertyToken) public {
    property = Property(_property);
    propertyToken = PropertyToken(_propertyToken);
  }


  // MODIFIERS

  modifier onlyOwner(uint256 _tokenId) {
    require(msg.sender == property.ownerOf(_tokenId), "Caller is not the owner");
    _;
  }

  modifier requireApproval(uint256 _tokenId) {
    require(keccak256(propertyData[_tokenId].requestByAddress[msg.sender].status) == keccak256("approved"), "Request not approved");
    _;
  }


  // EVENTS

  event Registered(uint256 indexed _tokenId);

  event Requested(uint256 indexed _tokenId, address indexed _requester);

  event Approved(uint256 indexed _tokenId, address indexed _requester);

  event Rejected(uint256 indexed _tokenId, address indexed _requester);

  event Canceled(uint256 indexed _tokenId, address indexed _requester);

  event CheckIn(uint256 indexed _tokenId, address indexed _requester);

  event CheckOut(uint256 indexed _tokenId, address indexed _requester);



  // PROPERTY FUNCTIONS

  function registerProperty(uint256 _tokenId, uint256 _price) external onlyOwner(_tokenId) {
    propertyData[_tokenId] = Data(_price, 0, true, address(0), new address[](0), new address[](0));
    registeredProperties.push(_tokenId);
    emit Registered(_tokenId);
  }

  function totalProperties() public view returns(uint256) {
    return registeredProperties.length;
  }

  function getPropertyPrice(uint256 _tokenId) public view returns(uint256) {
    return propertyData[_tokenId].price;
  }

  function getPropertyStays(uint256 _tokenId) public view returns(uint256) {
    return propertyData[_tokenId].stays;
  }

  function getPropertyOccupant(uint256 _tokenId) public view returns(address) {
    return propertyData[_tokenId].occupant;
  }

  function getPropertyRegistered(uint256 _tokenId) public view returns(bool) {
    return propertyData[_tokenId].registered;
  }

  function getRegisteredProperties() public view returns(uint256[]) {
    return registeredProperties;
  }


  // REQUEST FUNCTIONS

  function request(uint256 _tokenId, uint256 _checkin, uint256 _checkout) external {
    propertyData[_tokenId].requestByAddress[msg.sender] = Request("pending", _checkin, _checkout);
    requestedPropertiesByAddress[msg.sender].push(_tokenId);
    addToPendingRequests(_tokenId, msg.sender);
    emit Requested(_tokenId, msg.sender);
  }

  function getPendingRequests(uint256 _tokenId) external view returns(address[]) {
    return propertyData[_tokenId].pendingRequests;
  }

  function getApprovedRequests(uint256 _tokenId) external view returns(address[]) {
    return propertyData[_tokenId].approvedRequests;
  }

  function getRequestStatus(uint256 _tokenId) external view returns(string) {
    return propertyData[_tokenId].requestByAddress[msg.sender].status;
  }

  function getRequestCheckin(uint256 _tokenId) external view returns(uint256) {
    return propertyData[_tokenId].requestByAddress[msg.sender].checkin;
  }

  function getRequestCheckout(uint256 _tokenId) external view returns(uint256) {
    return propertyData[_tokenId].requestByAddress[msg.sender].checkout;
  }

  function approveRequest(uint256 _tokenId, address _requester) external onlyOwner(_tokenId) {
    propertyData[_tokenId].requestByAddress[_requester].status = "approved";
    removeFromPendingRequests(_tokenId, _requester);
    addToApprovedRequests(_tokenId, _requester);
    emit Approved(_tokenId, _requester);
  }

  function rejectRequest(uint256 _tokenId, address _requester) external onlyOwner(_tokenId) {
    propertyData[_tokenId].requestByAddress[_requester].status = "rejected";
    removeFromPendingRequests(_tokenId, _requester);
    emit Rejected(_tokenId, _requester);
  }

  function cancelRequest(uint256 _tokenId, address _requester) external onlyOwner(_tokenId) {
    propertyData[_tokenId].requestByAddress[_requester].status = "canceled";
    emit Canceled(_tokenId, _requester);
  }

  function getRequestedProperties() external view returns(uint256[]) {
    return requestedPropertiesByAddress[msg.sender];
  }

  // CHECKIN & CHECKOUT FUNCTIONS

  function checkin(uint256 _tokenId) external requireApproval(_tokenId) {
    // require(now >= propertyData[_tokenId].requestByAddress[msg.sender].checkin;, "Too soon");
    // require(now <= propertyData[_tokenId].requestByAddress[msg.sender].checkout;, "Too late");
    require(propertyToken.transferFrom(msg.sender, this, propertyData[_tokenId].price), "Guest payment failed");

    propertyData[_tokenId].occupant = msg.sender;
    propertyData[_tokenId].requestByAddress[msg.sender].status = "checkedIn";
    propertyData[_tokenId].stays += 1;
    emit CheckIn(_tokenId, msg.sender);
  }

  function checkout(uint256 _tokenId) external {
    require(propertyData[_tokenId].occupant == msg.sender, "Only the occupant can checkout");
    require(propertyToken.transfer(property.ownerOf(_tokenId), propertyData[_tokenId].price), "Could not complete transfer to property owner");

    propertyData[_tokenId].occupant = address(0);
    propertyData[_tokenId].requestByAddress[msg.sender].status = "checkedOut";
    emit CheckOut(_tokenId, msg.sender);
  }


  function addToPendingRequests(uint256 _tokenId, address _addr) private {
    uint256 id = propertyData[_tokenId].pendingRequests.length;
    propertyData[_tokenId].pendingRequestsIndexes[_addr] = id;
    propertyData[_tokenId].pendingRequests.push(_addr);
  }

  function removeFromPendingRequests(uint256 _tokenId, address _addr) private {
    uint256 id = propertyData[_tokenId].pendingRequestsIndexes[_addr];
    delete propertyData[_tokenId].pendingRequests[id];
  }

  function addToApprovedRequests(uint256 _tokenId, address _addr) private {
    uint256 id = propertyData[_tokenId].approvedRequests.length;
    propertyData[_tokenId].approvedRequestsIndexes[_addr] = id;
    propertyData[_tokenId].approvedRequests.push(_addr);
  }

  function removeFromApprovedRequests(uint256 _tokenId, address _addr) private {
    uint256 id = propertyData[_tokenId].approvedRequestsIndexes[_addr];
    delete propertyData[_tokenId].approvedRequests[id];
  }
}
