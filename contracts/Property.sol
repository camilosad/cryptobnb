pragma solidity ^0.4.22;

import "openzeppelin-solidity/contracts/token/ERC721/ERC721Token.sol";

contract Property is ERC721Token {
  
  constructor() ERC721Token("Property", "PPTY") public {}

  modifier onlyOwner(uint256 _tokenId) {
    require(tokenOwner[_tokenId] == msg.sender, "Caller is not the owner");
    _;
  }

  function createProperty(string _uri) external {
    uint256 newToken = allTokens.length + 1;
    _mint(msg.sender, newToken);
    if (bytes(_uri).length != 0) {
      _setTokenURI(newToken, _uri);
    }
  }

  function setURI(uint256 _tokenId, string _uri) external onlyOwner(_tokenId) {
    _setTokenURI(_tokenId, _uri);
  }

  function getProperties() external view returns(uint256[]) {
    return ownedTokens[msg.sender];
  }
}
