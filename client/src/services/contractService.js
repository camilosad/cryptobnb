import TruffleContract from 'truffle-contract'

import propertyJson from './../contracts/property';
import propertyRegistryJson from './../contracts/property_registry';
import propertyTokenJson from './../contracts/property_token';


const getContract = (json, web3 = window.web3) => {
  const contract = TruffleContract(json);
  contract.setProvider(web3.currentProvider);
  //dirty hack for web3@1.0.0 support for localhost testrpc, see https://github.com/trufflesuite/truffle-contract/issues/56#issuecomment-331084530
  if (typeof contract.currentProvider.sendAsync !== "function") {
    contract.currentProvider.sendAsync = function() {
      return contract.currentProvider.send.apply(
        contract.currentProvider, arguments
      );
    };
  }

  return contract.deployed();
}

export const fetchPropertyContract = () => (getContract(propertyJson));

export const fetchPropertyRegistryContract = () => (getContract(propertyRegistryJson));

export const fetchPropertyTokenContract = () => (getContract(propertyTokenJson));

// syntax from web3 1.0 docs
// export const fetchPropertyContract = () => (new window.web3.eth.Contract(propertyJson.abi));