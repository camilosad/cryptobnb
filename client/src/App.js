import './App.css';
import React, { Component } from 'react';
import { Layout, Menu, Breadcrumb, Icon } from 'antd';
import { initializeWeb3, fetchAccounts } from './web3';
import { fetchPropertyContract, fetchPropertyRegistryContract, fetchPropertyTokenContract } from './services/contractService';
import Dashboard from './components/Dashboard';
import NewProperty from './components/NewProperty';
import MyProperty from './components/MyProperty';
import MyRequests from './components/MyRequests';


class App extends Component {
  constructor(props) {
    super(props);
    initializeWeb3();
    
    this.state = {
      propertyContract: {},
      propertyRegistryContract: {},
      propertyTokenContract: {},
      currentAccount: null,
      sideNavCollapsed: false,
      currentTab: 'dashboard',
      properties: []
    }
  }

  componentDidMount() {
    this.fetchData();
  }
  
  render() {
    const { Header, Content, Footer, Sider } = Layout;
    const { SubMenu } = Menu;
    const { sideNavCollapsed, currentTab } = this.state

    return (
      <div className="App">
        <Layout style={{ minHeight: '100vh' }}>
        <Sider collapsible collapsed={sideNavCollapsed} onCollapse={this.toggleSideNav}>
          <div className="logo" />
          <Menu theme="dark" defaultSelectedKeys={['dashboard']} mode="inline" onClick={this.switchTab}>
            <Menu.Item key="dashboard">
              <Icon type="appstore" />
              <span>Dashboard</span>
            </Menu.Item>
            <Menu.Item key="new_property">
              <Icon type="home" />
              <span>New Property</span>
            </Menu.Item>
            <Menu.Item key="my_requests">
              <Icon type="schedule" />
              <span>My Requests</span>
            </Menu.Item>
            <SubMenu key="my_properties" title={<span><Icon type="bars" /><span>My Properties</span></span>}>
              {this.renderPropertiesMenu()}
            </SubMenu>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }} />
          <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>{currentTab}</Breadcrumb.Item>
            </Breadcrumb>
            <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
              {this.renderContent()}
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            CryptoBnB
          </Footer>
        </Layout>
      </Layout>
      </div>
    );
  }

  toggleSideNav = () => {
    this.setState({ sideNavCollapsed: !this.state.sideNavCollapsed });
  }

  switchTab = ({ item, key, selectedKeys }) => {
    this.setState({ currentTab: key });
  }

  fetchData = () => {
    Promise.all([
      fetchPropertyContract(), 
      fetchPropertyRegistryContract(), 
      fetchPropertyTokenContract(),
      fetchAccounts()
    ]).then(data => {
      this.setState({
        propertyContract: data[0],
        propertyRegistryContract: data[1],
        propertyTokenContract: data[2],
        currentAccount: data[3][0]
      }, this.fetchProperties)
    })
  }

  fetchProperties = () => {
    const { currentAccount, propertyContract } = this.state;
    if (!currentAccount || !propertyContract) { return null }

    propertyContract.getProperties({ from: currentAccount, gas: 250000 })
      .then(properties => this.setState({ properties: properties.map(p => p.toNumber()) }))
      .catch(error => console.log(error))
  }

  renderPropertiesMenu = () => {
    const { properties } = this.state;
    return properties.map(property => (
      <Menu.Item key={`property-${property}`}>Property {property}</Menu.Item>
    ));
  }

  renderContent = () => {
    if (!this.dataLoaded()) { return null }
    const { currentTab, currentAccount, propertyContract, propertyRegistryContract } = this.state;
    
    if (currentTab === "dashboard") { 
      return (
        <Dashboard 
          account={currentAccount}
          propertyContract={propertyContract}
          propertyRegistryContract={propertyRegistryContract} />
      )
    }

    if (currentTab === "new_property") {
      return (
        <NewProperty 
          account={currentAccount} 
          propertyContract={propertyContract} />
      )
    }

    if (currentTab === "my_requests") {
      return (
        <MyRequests 
          account={currentAccount}
          propertyContract={propertyContract}
          propertyRegistryContract={propertyRegistryContract} />
      )
    }

    if (currentTab.startsWith('property-')) {
      const propertyToken = currentTab.split('-')[1];
      return (
        <MyProperty
          account={currentAccount}
          propertyToken={propertyToken}
          propertyContract={propertyContract}
          propertyRegistryContract={propertyRegistryContract} />
      )
    }

    return <div>No content for this tab just yet</div>
  }

  dataLoaded = () => {
    const { currentTab, propertyContract, propertyRegistryContract, currentAccount } = this.state;
    return currentTab && propertyContract && propertyRegistryContract && currentAccount;
  }
}

export default App;
