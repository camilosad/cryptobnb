import React, { Component } from 'react';
import { Form, Icon, Input, Button } from 'antd';

class NewProperty extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      transaction: null,
      uri: null
    }
  }
  
  render() {
    const { Item: FormItem } = Form;
    const { uri } = this.state;

    return (
      <Form layout="inline" onSubmit={this.handleSubmit}>
        <FormItem>
          <Input 
            prefix={<Icon type="link" style={{ color: 'rgba(0,0,0,.25)' }} />} 
            placeholder="URI"
            onChange={this.changeURI}
            onPressEnter={this.handleSubmit} />
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit" disabled={!uri}>
            Create Property
          </Button>
        </FormItem>
      </Form>
    )
  }

  handleSubmit = () => {
    this.createProperty();
  }

  createProperty = () => {
    const { account, propertyContract } = this.props;
    const { uri } = this.state;
    propertyContract.createProperty(uri, { from: account })
  }

  changeURI = (e) => {
    this.setState({ uri: e.target.value });
  }
}

export default Form.create()(NewProperty);