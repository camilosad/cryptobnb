import React, { Component } from 'react';
import { Divider } from 'antd';
import PropertiesList from './PropertiesList';

export default class Dashboard extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      propertyTokens: []
    }
  }

  componentDidMount() {
    this.fetchPropertyTokens();
  }
  
  render() {
    const { propertyContract, propertyRegistryContract, account } = this.props;
    const { propertyTokens } = this.state;

    return (
      <React.Fragment>
        <h1>Listed Properties</h1>
        <Divider />
        <PropertiesList
          account={account}
          propertyTokens={propertyTokens}
          propertyContract={propertyContract}
          propertyRegistryContract={propertyRegistryContract} />
      </React.Fragment>
    )
  }

  fetchPropertyTokens = () => {
    const { propertyRegistryContract } = this.props;
    
    propertyRegistryContract
      .getRegisteredProperties()
      .then(this.setPropertyTokens)
  }

  setPropertyTokens = (tokens) => {
    this.setState({ propertyTokens: tokens.map(t => t.toNumber()) });
  }
}