import React, { Component } from 'react';
import moment from 'moment';
import { List, Icon, Button, Modal, DatePicker, Skeleton } from 'antd';
import houseImgs from './../assets/images/houses';

const IconText = ({ type, text }) => (
  <span>
    <Icon type={type} style={{ marginRight: 8 }} />
    {text}
  </span>
);


export default class PropertyListing extends Component {
  
  constructor(props) {
    super(props);
    
    this.state = {
      propertyStays: null,
      propertyPrice: null,
      propertyOccupant: '0x0',
      propertyURI: null,
      requestModalOpen: false,
      requestCheckIn: null,
      requestCheckOut: null,
      loaded: false
    }
  }

  componentDidMount() {
    this.fetchPropertyData();
  }

  render() {
    const { propertyToken } = this.props;
    const { requestModalOpen, requestCheckIn, requestCheckOut, loaded } = this.state;
    const { RangePicker } = DatePicker;

    if (!loaded) { return <Skeleton active /> }

    return (
      <React.Fragment>
        <List.Item key={propertyToken} actions={this.actions()} extra={this.listingImg()}>
          <List.Item.Meta title={this.propertyURI()} description={this.currentOccupant()} />
          <Button onClick={this.openRequestModal}>Request</Button>
        </List.Item>
        <Modal title="New Request"
          visible={requestModalOpen}
          onOk={this.submitRequest}
          onCancel={this.cancelRequest} >
          <div>
            <RangePicker
              disabledDate={this.disabledDate}
              onChange={this.setRequestDates}
              value={[requestCheckIn, requestCheckOut]}
              format="YYYY-MM-DD" />
          </div>
        </Modal>
      </React.Fragment>
    )
  }

  currentOccupant = () => {
    const { propertyOccupant } = this.state;
    if (propertyOccupant.startsWith('0x0')) {
      return "This property is currently available";
    }
    
    return `Current Occupant: ${propertyOccupant}`;
  }

  propertyURI = () => {
    const { propertyURI } = this.state
    return (<a href="http://google.ca">{propertyURI}</a>);
  }

  actions = () => {
    const { propertyStays, propertyPrice } = this.state;
    
    return([
      <IconText type="team" text={propertyStays} />,
      <IconText type="barcode" text={`${Number(propertyPrice).toFixed(2)} per night`} />
    ])
  }

  listingImg = () => {
    const randomHouse = houseImgs[Math.floor(Math.random()* houseImgs.length)]
    return (<img width={272} alt="logo" src={randomHouse} />);
  }


  fetchPropertyData = () => {
    const { propertyContract, propertyRegistryContract, propertyToken } = this.props;
    
    Promise.all([
      propertyRegistryContract.getPropertyStays(propertyToken),
      propertyRegistryContract.getPropertyPrice(propertyToken),
      propertyRegistryContract.getPropertyOccupant(propertyToken),
      propertyContract.tokenURI(propertyToken),
    ]).then(data => {
      this.setState({ 
        propertyStays: data[0].toNumber(),
        propertyPrice: data[1].toNumber(),
        propertyOccupant: data[2],
        propertyURI: data[3],
        loaded: true
      })
    })
  }

  totalPrice = () => {
    const { propertyPrice, requestCheckOut, requestCheckIn } = this.state;
    if (!(requestCheckIn && requestCheckOut)) { return null }
    
    const days = requestCheckOut.diff(requestCheckIn, 'days');
    return days * propertyPrice;
  }

  openRequestModal = () => {
    this.setState({ requestModalOpen: true });
  }

  closeRequestModal = () => {
    this.setState({ requestModalOpen: false });
  }

  submitRequest = () => {
    const { propertyRegistryContract, propertyToken, account } = this.props;
    const { requestCheckIn, requestCheckOut } = this.state;
    this.closeRequestModal();

    propertyRegistryContract
      .request(propertyToken, requestCheckIn.valueOf(), requestCheckOut.valueOf(), { from: account })
      .then(console.log)
  }

  clearRequestDates = () => {
    this.setState({ requestCheckIn: null, requestCheckOut: null });
  }

  setRequestDates = (dates) => {
    const requestCheckIn = dates[0];
    const requestCheckOut = dates[1];
    this.setState({ requestCheckIn, requestCheckOut });
  }

  cancelRequest = () => {
    this.clearRequestDates();
    this.closeRequestModal();
  }

  disabledDate = current => (
    current && current < moment().endOf('day')
  )

  
}