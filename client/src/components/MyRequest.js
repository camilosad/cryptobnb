import React, { Component } from 'react';
import moment from 'moment';
import { List, Icon, Button, Tag } from 'antd';
import houseImgs from './../assets/images/houses';

const IconText = ({ type, text }) => (
  <span>
    <Icon type={type} style={{ marginRight: 8 }} />
    {text}
  </span>
);


export default class MyRequest extends Component {
  
  constructor(props) {
    super(props);
    
    this.state = {
      propertyPrice: null,
      propertyURI: null,
      requestCheckIn: null,
      requestCheckOut: null
    }
  }

  componentDidMount() {
    this.fetchPropertyData();
    this.fetchRequestData();
  }

  render() {
    const { propertyToken, propertyURI } = this.state;

    return (
      <React.Fragment>
        <List.Item key={propertyToken} actions={this.actions()} extra={this.propertyImg()}>
          <List.Item.Meta title={propertyURI} description="" />
          {this.requestOptions()}
        </List.Item>
      </React.Fragment>
    )
  }

  requestOptions = () => {
    if (this.canCheckIn()) {
      return (
        <div>
          <span style={{ marginRight: '8px' }}>
            <Button onClick={this.checkIn}>Check In</Button>
          </span>
          <span style={{ marginLeft: '8px' }}>
            <Button type="danger" onClick={this.cancelRequest}>Cancel</Button>
          </span>
        </div>
      )
    }

    if (this.canCheckOut()) {
      return (
        <span style={{ marginLeft: '8px' }}>
          <Button onClick={this.checkOut}>Check Out</Button>
        </span>
      )
    }

    if (this.requestCanceled()) { return null }

    return (
      <span style={{ marginLeft: '8px' }}>
        <Button type="danger" onClick={this.cancelRequest}>Cancel</Button>
      </span>
    );
  }

  checkIn = () => {

  }

  cancelRequest = () => {

  }

  checkOut = () => {

  }

  requestCanceled = () => {
    const { requestStatus } = this.state;
    return requestStatus === "canceled";
  }

  canCheckOut = () => (
    this.checkedIn() && this.beforeCheckout()
  )

  canCheckIn = () => (
    this.afterCheckIn() && this.requestApproved()
  )

  checkedIn = () => {
    const { requestStatus } = this.state;
    return requestStatus === "checkedIn";
  }

  requestApproved = () => {
    const { requestStatus } = this.state;
    return requestStatus === "approved";
  }

  afterCheckIn = () => {
    const { requestCheckIn } = this.state;
    return moment().isSameOrAfter(moment(requestCheckIn));
  }

  beforeCheckout = () => {
    const { requestCheckOut } = this.state;
    return moment().isSameOrBefore(moment(requestCheckOut));
  }

  fetchPropertyData = () => {
    const { account, propertyToken, propertyContract, propertyRegistryContract } = this.props;
    
    Promise.all([
      propertyContract.tokenURI(propertyToken, { from: account }),
      propertyRegistryContract.getPropertyPrice(propertyToken, { from: account })
    ]).then(data => {
      this.setState({ 
        propertyURI: data[0],
        propertyPrice: data[1].toNumber()
      })
    })
  }

  fetchRequestData = () => {
    const { account, propertyToken, propertyRegistryContract } = this.props;
    
    Promise.all([
      propertyRegistryContract.getRequestStatus(propertyToken, { from: account }),
      propertyRegistryContract.getRequestCheckin(propertyToken, { from: account }),
      propertyRegistryContract.getRequestCheckout(propertyToken, { from: account })
    ]).then(data => {
      this.setState({ 
        requestStatus: data[0],
        requestCheckIn: data[1].toNumber(),
        requestCheckOut: data[2].toNumber(),
      })
    })
  }

  actions = () => {
    const { requestStatus, propertyPrice } = this.state;
    
    return([
      <Tag color={this.statusColor()}>{requestStatus}</Tag>,
      <IconText type="calendar" text={this.requestRange()} />,
      <IconText type="barcode" text={`${Number(propertyPrice).toFixed(2)} per night`} />
    ])
  }

  statusColor = () => {
    const { requestStatus } = this.state;
    if (requestStatus === "pending") { return "gold" }
    if (requestStatus === "approved") { return "green" }
    if (requestStatus === "rejected") { return "red" }
    return "geekblue";
  }

  requestRange = () => {
    const { requestCheckIn, requestCheckOut } = this.state;
    const startDate = moment(requestCheckIn).format("MMM do, YYYY");
    const endDate = moment(requestCheckOut).format("MMM do, YYYY");
    return `${startDate} - ${endDate}`;
  }

  propertyImg = () => {
    const randomHouse = houseImgs[Math.floor(Math.random()* houseImgs.length)]
    return (<img width={272} alt="logo" src={randomHouse} />);
  }
  
}