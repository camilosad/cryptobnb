import React, { Component } from 'react';
import { List } from 'antd';
import MyRequest from './MyRequest';

export default class RequestsList extends Component {
  
  render() {    
    return (
      <React.Fragment>
        <List itemLayout="vertical">
          {this.renderRequestItems()}
        </List>
      </React.Fragment>
    )
  }
  
  renderRequestItems = () => {
    const { propertyContract, propertyRegistryContract, account, requestedProperties } = this.props;
    
    return requestedProperties.map(request => (
      <MyRequest
        key={request}
        account={account}
        propertyToken={request}
        propertyContract={propertyContract}
        propertyRegistryContract={propertyRegistryContract} />
    ));
  }
}