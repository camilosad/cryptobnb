import React, { Component } from 'react';
import { Form, InputNumber, Button, Divider, Row, Col, Tag, Skeleton } from 'antd';
import MyPropertyRequestList from './MyPropertyRequestList';

export default class MyProperty extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      propertyURI: null,
      propertyPrice: null,
      propertyStays: null,
      propertyOccupant: null,
      propertyRegistered: false,
      pendingRequests: [],
      approvedRequests: [],
      rejectedRequests: [],
      canceledRequests: [],
      loaded: false
    }
  }

  componentDidMount() {
    const { propertyToken } = this.props;
    this.fetchPropertyData(propertyToken);
    // this.watchRegistered(propertyToken);
  }

  componentWillReceiveProps(nextProps) {
    const { propertyToken } = this.props;
    if (propertyToken !== nextProps.propsToken) {
      this.fetchPropertyData(nextProps.propertyToken);
      // this.watchRegistered(propertyToken);
    }
  }
  
  render() {
    const { propertyStays, propertyURI, propertyPrice, loaded } = this.state;
    if (!loaded) { return <Skeleton active /> }

    return (
      <React.Fragment>
        <Row>
          <Col span={8}><h2>{propertyStays} stays</h2></Col>
          <Col span={8}><h1>{propertyURI}</h1></Col>
          <Col span={8}><h2>$ {propertyPrice}</h2></Col>
        </Row>
        {this.propertyStatus()}
        <Divider />
        {this.renderPropertyContent()}
      </React.Fragment>
    )
  }

  renderPropertyContent = () => {
    const { propertyRegistered } = this.state;
    if (propertyRegistered) { return this.propertyRequests() }
    return this.registerPropertyForm();
  }

  propertyRequests = () => {
    const { pendingRequests, approvedRequests, rejectedRequests, canceledRequests } = this.state;
    
    return (
      <MyPropertyRequestList
        pendingRequests={pendingRequests}
        approvedRequests={approvedRequests}
        rejectedRequests={rejectedRequests}
        canceledRequests={canceledRequests} />
    )
  }
 
  propertyStatus = () => {
    const { propertyRegistered, propertyOccupant } = this.state;
    const propertyAvailable = propertyOccupant.startsWith('0x0');
    
    if (!propertyRegistered) {
      return <Tag color="orange">Not Registered</Tag>
    }
    
    if (propertyAvailable) {
      return <Tag color="green">Currently Available</Tag>
    }

    return (
      <Tag color="geekblue">{`Current Guest: ${propertyOccupant}`}</Tag>
    )
  }
  
  registerPropertyForm = () => {
    const { propertyPrice } = this.state;
    
    return (
      <Form layout="inline" onSubmit={this.handleSubmit}>
        <Form.Item>
          <InputNumber
            min={0}
            step={10}
            precision={2}
            defaultValue={propertyPrice}
            formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            parser={value => value.replace(/\$\s?|(,*)/g, '')}
            onChange={this.changePrice}
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Register Property
          </Button>
        </Form.Item>
      </Form>
    )
  }

  changePrice = (value) => {
    this.setState({ propertyPrice: value });
  }

  handleSubmit = () => {
    this.registerProperty();
  }

  fetchPropertyData = (propertyToken) => {
    const { account, propertyContract, propertyRegistryContract } = this.props;
    
    Promise.all([
      propertyContract.tokenURI(propertyToken, { from: account }),
      propertyRegistryContract.getPropertyStays(propertyToken, { from: account }),
      propertyRegistryContract.getPropertyPrice(propertyToken, { from: account }),
      propertyRegistryContract.getPropertyOccupant(propertyToken, { from: account }),
      propertyRegistryContract.getPropertyRegistered(propertyToken, { from: account }),
      propertyRegistryContract.getPendingRequests(propertyToken, { from: account }),
      propertyRegistryContract.getApprovedRequests(propertyToken, { from: account }),
    ]).then(data => {
      this.setState({ 
        propertyURI: data[0],
        propertyStays: data[1].toNumber(),
        propertyPrice: data[2].toNumber(),
        propertyOccupant: data[3],
        propertyRegistered: data[4],
        pendingRequests: data[5],
        approvedRequests: data[6],
        loaded: true
      })
    })
  }

  registerProperty = () => {
    const { account, propertyRegistryContract, propertyToken } = this.props;
    const { propertyPrice } = this.state;
    propertyRegistryContract.registerProperty(propertyToken, propertyPrice, { from: account })
    this.watchRegistered()
  }

  watchRegistered = (propertyToken) => {
    const { propertyRegistryContract } = this.props;

    propertyRegistryContract.Registered(
      { _tokenId: propertyToken },
      { fromBlock: 0, toBlock: 'latest' }, 
      this.propertyRegistered
    );
  }

  propertyRegistered = (error, result) => {
    console.log(error, result.args._tokenId.toNumber());
  }

}