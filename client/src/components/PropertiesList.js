import React, { Component } from 'react';
import { List } from 'antd';
import PropertyListing from './PropertyListing';

export default class PropertyList extends Component {
  
  render() {    
    return (
      <React.Fragment>
        <List itemLayout="vertical">
          {this.renderPropertyListings()}
        </List>
      </React.Fragment>
    )
  }
  
  renderPropertyListings = () => {
    const { propertyContract, propertyRegistryContract, propertyTokens, account } = this.props;
    
    return propertyTokens.map(token => (
      <PropertyListing
        key={token}
        account={account}
        propertyToken={token}
        propertyContract={propertyContract}
        propertyRegistryContract={propertyRegistryContract} />
    ));
  }
}