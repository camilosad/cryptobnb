import React, { Component } from 'react';
import { Divider } from 'antd';
import RequestsList from './RequestsList';

export default class MyRequests extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      requestedProperties: []
    }
  }

  componentDidMount() {
    this.fetchRequests();
  }

  render() {
    const { account, propertyContract, propertyRegistryContract } = this.props;
    const { requestedProperties } = this.state;

    return (
      <React.Fragment>
        <h1>My Requests</h1>
        <Divider />
        <RequestsList
          account={account}
          requestedProperties={requestedProperties}
          propertyContract={propertyContract}
          propertyRegistryContract={propertyRegistryContract} />
      </React.Fragment>
    )
  }

  fetchRequests = () => {
    const { account, propertyRegistryContract } = this.props;
    
    propertyRegistryContract
      .getRequestedProperties({ from: account })
      .then(tokens => {
        this.setState({ requestedProperties: tokens.map(token => token.toNumber()) })
      })
  }
}