import React, { Component } from 'react';
import { Tabs, List } from 'antd';

export default class MyPropertyRequestList extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      activeTab: "pending"
    }
  }
  
  render() {
    const { activeTab } = this.state;

    return (
      <Tabs defaultActiveKey={activeTab} onChange={this.onChangeTab}>
        <Tabs.TabPane tab="Pending" key="pending">{this.requestsList()}</Tabs.TabPane>
        <Tabs.TabPane tab="Approved" key="approved">{this.requestsList()}</Tabs.TabPane>
        <Tabs.TabPane tab="Rejected" key="rejected">{this.requestsList()}</Tabs.TabPane>
        <Tabs.TabPane tab="Canceled" key="canceled">{this.requestsList()}</Tabs.TabPane>
      </Tabs>
    )
  }

  requestsList = () => (
    <React.Fragment>
      <List itemLayout="vertical">
        {this.renderRequestItems()}
      </List>
    </React.Fragment>
  )

  displayedRequests = () => {
    const { pendingRequests, approvedRequests, rejectedRequests, canceledRequests } = this.props;
    const { activeTab } = this.state;
    if (activeTab === "pending") return pendingRequests;
    if (activeTab === "approved") return approvedRequests;
    if (activeTab === "rejected") return rejectedRequests;
    if (activeTab === "canceled") return canceledRequests;
    return [];
  }

  renderRequestItems = () => (
    this.displayedRequests().map(request => this.renderRequestItem(request))
  )

  renderRequestItem = (request) => (
    <React.Fragment>
      <List.Item key={request} >
        <List.Item.Meta title={request} description="" />
        {/* {this.requestOptions()} */}
      </List.Item>
    </React.Fragment>
  )

  onChangeTab = (key) => {
    this.setState({ activeTab: key })
  }

  // fetchPropertyRequests = () => {
  //   const { propertyToken, propertyRegistryContract } = this.props;
  //   debugger;
    
  //   Promise.all([
  //     propertyRegistryContract.getPendingRequests(propertyToken),
  //     propertyRegistryContract.getApprovedRequests(propertyToken)
  //   ]).then(data => {
  //     this.setState({
  //       pendingRequests: data[0],
  //       approvedRequests: data[1]
  //     })
  //   })
  // }
  
  // renderRequestItem = () => {
  //   const { propertyContract, propertyRegistryContract, account, requestedProperties } = this.props;
    
  //   return requestedProperties.map(request => (
  //     <MyRequest
  //       key={request}
  //       account={account}
  //       propertyToken={request}
  //       propertyContract={propertyContract}
  //       propertyRegistryContract={propertyRegistryContract} />
  //   ));
  // }
}